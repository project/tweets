<?php

/**
 * @file
 * Creates the Tweets block for a specified username.
 */

class TweetsBlock {
  private $username;
  private $count;
  protected $tweets;
  protected $header = NULL;
  protected $footer = NULL;
  private $show_images = TRUE;
  private $show_dates = TRUE;

  public function __construct($settings) {
    $this->username = $settings['username'];
    $this->count = $settings['count'];
    $this->header = isset($settings['header']) ? $settings['header'] : NULL;
    $this->footer = isset($settings['footer']) ? $settings['footer'] : NULL;
    $this->show_images = $settings['show_profile_images'];
    $this->show_dates = $settings['show_dates'];
    $this->list_type = isset($settings['list_type']) ? $settings['list_type'] : 'ol';

    $this->getTweets();

    $path = drupal_get_path('module', 'tweets');
    drupal_add_css($path . '/tweets.css');
  }

  public function __toString() {
    $output = '';

    if (!empty($this->tweets)) {
      foreach ($this->tweets as $tweet) {
        if ($this->show_images) {
          $profile_image_url = $GLOBALS['is_https'] ? $tweet->user->profile_image_url_https : $tweet->user->profile_image_url;
          $profile_image = theme('image', array('path' => $profile_image_url));
        }
        else {
          $profile_image = NULL;
        }

        if ($this->show_dates) {
          $post_date = t('!time ago', array('!time' => format_interval(time() - strtotime($tweet->created_at))));
        }
        else {
          $post_date = NULL;
        }

        $tweet_classes = array('tweet');
        $tweet_classes[] = $this->show_images ? 'with-image' : 'no-image';
        $tweet_classes[] = $this->show_dates ? 'with-date' : 'no-date';

        $items[] = array(
          'data' => theme('tweet', array(
            'tweet' => $this->twitterify($tweet->text),
            'image' => $profile_image,
            'date' => $post_date,
          )),
          'class' => $tweet_classes,
        );
      }

      $number_of_tweets = count($items);

      if ($number_of_tweets === 1) {
        $item = $items[0];

        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $tweet = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }

        $tweets = theme('tweets_single_tweet', array(
          'tweet' => $tweet,
          'attributes_array' => $attributes,
        ));
      }
      elseif ($number_of_tweets > 1) {
        $tweets = theme('item_list', array(
          'items' => $items,
          'type' => $this->list_type,
          'attributes' => array(
            'class' => 'tweets',
          ),
        ));
      }

      $output .= theme('tweets', array(
        'tweets' => $tweets,
        'header' => $this->header,
        'footer' => $this->footer,
      ));
    }

    return $output;
  }

  /**
   * Get tweets - either from the Twitter API, or from Drupal's cache.
   */
  private function getTweets() {
    // Attempt to load the tweets from the cache.
    if ($cache = cache_get('tweets')) {
      $this->tweets = $cache->data;
    }
    // Query the Twitter API.
    else {
      $response = drupal_http_request('https://api.twitter.com/1/statuses/user_timeline.json?include_entities=true&include_rts=true&screen_name=' . $this->username);

      if ($response->code == 200) {
        $data = json_decode($response->data);

        // Store the results in cache, if configured to do so.
        if ($cache_duration = variable_get('tweets_cache', 0)) {
          cache_set('tweets', $data, 'cache', $cache_duration);
        }

        $this->tweets = $data;
      }
    }

    // Limit the number of tweets to the specified value.
    if (!empty($this->tweets)) {
      $this->tweets = array_slice($this->tweets, 0, $this->count);
    }
  }

  /**
   * Turn URLs, usernames, hashtags etc into clickable links.
   */
  private function twitterify($text) {
    $text = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" >\\2</a>", $text);
    $text = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" >\\2</a>", $text);
    $text = preg_replace("/@(\w+)/", "<a href=\"http://twitter.com/\\1\" >@\\1</a>", $text);
    $text = preg_replace("/#(\w+)/", "<a href=\"http://search.twitter.com/search?q=\\1\" >#\\1</a>", $text);

    return $text;
  }
}

