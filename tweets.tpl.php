<?php
/**
 * @file
 * Displays the tweets.
 *
 * Available variables:
 * - $tweets: The a formatted ordered list of tweets.
 * - $header: Any defined header text.
 * - $footer: Any defined footer text.
 *
 * @see template_preprocess_tweets()
 * @see template_process_tweets()
 *
 * @ingroup themeable
 */
?><?php if ($header): ?><div class="tweets-header"><?php print $header; ?></div><?php endif; ?>
<?php print $tweets; ?>
<?php if ($footer): ?><div class="tweets-footer"><?php print $footer; ?></div><?php endif; ?>
