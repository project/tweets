
ABOUT THIS MODULE
-----------------

This module uses the Twitter API
(https://dev.twitter.com/docs/api/1/get/statuses/user_timeline) to create a
simple block that displays a specified number of tweets by a specified user.

INSTALLATION
------------

See http://drupal.org/documentation/install/modules-themes.

USAGE
-----

 * Download and enable the module.
 * Go to admin/structure/block/manage/tweets/tweets/configure, edit your
   twitter username and count (unless you want the five latest tweets from
   @drupal), and place the block in a region.
 * Go to admin/config/development/performance and configure the caching setting
   for the block.

AUTHOR
------

Oliver Davies
oliver@oliverdavies.co.uk
