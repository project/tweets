<?php
/**
 * @file
 * Displays a tweet.
 *
 * Available variables:
 * - $tweet: The text for the tweet.
 * - $date: The date that the tweet was posted.
 * - $image: The poster's Twitter profile image.
 *
 * @see template_preprocess_tweet()
 * @see template_process_tweet()
 *
 * @ingroup themeable
 */
?><blockquote class="tweet-tweet"><p><?php print $tweet; ?></p></blockquote>
<?php if ($date): ?><div class="tweet-date">- <?php print $date; ?></div><?php endif; ?>
<?php if ($image): ?><div class="tweet-profile-image"><?php print $image; ?></div><?php endif; ?>
